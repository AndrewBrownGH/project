<?php

declare(strict_types=1);

namespace FixedCostStrategy;

require_once "employee.php";
require_once "cost_strategy.php";

use CostStrategy\CostStrategy;
use Employee\Employee;

class FixedCostStrategy implements CostStrategy
{
    protected int $month;

    protected int $payment;

    public function __construct(int $payment, int $month)
    {
        $this->payment = $payment;
        $this->month = $month;
    }

    public function chargeType(): string
    {
        return 'Fixed payment';
    }

    public function cost(): int
    {
        return $this->month * $this->payment;
    }
}