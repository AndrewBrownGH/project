<?php

declare(strict_types=1);

namespace Employee;

require_once "human.php";
require_once "cost_strategy.php";

use Human\Human;
use CostStrategy\CostStrategy;

class Employee extends Human
{
    protected int $duration;

    protected int $payment;

    protected CostStrategy $costStrategy;

    public function __construct(
        string $name, string $surname, string $patronymic, CostStrategy $costStrategy
    )
    {
        parent::__construct($name, $surname, $patronymic);
        $this->costStrategy = $costStrategy;
    }

    public function cost(): int
    {
        return $this->costStrategy->cost();
    }

    public function chargeType() : string
    {
        return $this->costStrategy->chargeType();
    }

    public function getPayment(): int
    {
        return $this->payment;
    }

    public function setPayment(int $payment): void
    {
        $this->payment = $payment;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }
}
