<?php

declare(strict_types=1);

namespace Company;

require_once 'project.php';
require_once 'timed_cost_strategy.php';
require_once 'fixed_cost_strategy.php';

use Employee\Designer\Designer;
use Employee\Developer\Developer;
use Employee\Typesetter\Typesetter;
use FixedCostStrategy\FixedCostStrategy;
use Project\Project;
use TimedCostStrategy\TimedCostStrategy;

$project = new Project('X');

$designer = new Designer(
    'alex', 'titov', 'lazarev', new FixedCostStrategy(3000, 1)
);

$seniorDeveloper = new Developer(
    'andrew', 'brown', 'vladimirovich', new TimedCostStrategy(60,10)
);

$firstMiddleDeveloper = new Developer(
    'nikita', 'lazhevich', 'test', new FixedCostStrategy(1000,3)
);

$secondMiddleDeveloper = new Developer(
    'sherlock', 'holmes', '221B', new FixedCostStrategy(1000,3)
);

$typesetter = new TypeSetter(
    'john', 'watson', '221B', new TimedCostStrategy(5,120)
);

$project->addEmployee($designer);
$project->addEmployee($seniorDeveloper);
$project->addEmployee($firstMiddleDeveloper);
$project->addEmployee($secondMiddleDeveloper);
$project->addEmployee($typesetter);

echo $project->totalCost();
