<?php

declare(strict_types=1);

namespace CostStrategy;

require_once "employee.php";

use Employee\Employee;

interface CostStrategy
{
    public function cost() : int;

    public function chargeType() : string;
}