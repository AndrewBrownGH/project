<?php

declare(strict_types=1);

namespace TimedCostStrategy;

require_once "employee.php";
require_once "cost_strategy.php";

use CostStrategy\CostStrategy;

class TimedCostStrategy implements CostStrategy
{
    protected int $hours;

    protected int $payment;

    public function __construct(int $payment, int $hours)
    {
        $this->payment = $payment;
        $this->hours = $hours;
    }

    public function chargeType(): string
    {
        return 'Timed payment';
    }

    public function cost(): int
    {
        return $this->hours * $this->payment;
    }
}