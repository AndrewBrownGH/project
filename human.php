<?php

declare(strict_types=1);

namespace Human;

abstract class Human
{
    protected static string $name;

    protected static string $surname;

    protected static string $patronymic;

    public function __construct(string $name, string $surname, string $patronymic)
    {
        self::$name = $name;
        self::$surname = $surname;
        self::$patronymic = $patronymic;
    }

    public static function getName(): string
    {
        return self::$name;
    }

    public static function setName(string $name): void
    {
        self::$name = $name;
    }

    public static function getSurname(): string
    {
        return self::$surname;
    }

    public static function setSurname(string $surname): void
    {
        self::$name = $surname;
    }

    public static function getPatronymic(): string
    {
        return self::$patronymic;
    }

    public static function setPatronymic(string $patronymic): void
    {
        self::$name = $patronymic;
    }

}