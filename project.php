<?php

declare(strict_types=1);

namespace Project;

use Employee\Employee;

require_once "designer.php";
require_once 'developer.php';
require_once 'typesetter.php';

class Project
{
    protected string $name;

    protected static $employees = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function addEmployee(Employee $employee): void
    {
        self::$employees[] = $employee;
    }

    public function totalCost(): int
    {
        $totalCost = 0;
        foreach (self::$employees as $employee)
        {
            $totalCost += $employee->cost();
        }
        return $totalCost;
    }

}